import sqlite3, hashlib, random, os,string
from flask import Flask, render_template, request, redirect, url_for, flash, session, make_response

from datetime import datetime, timedelta
from time import sleep


app = Flask(__name__)
app.debug = True
app.permanent_session_lifetime = timedelta(minutes=5)

app.config['SECRET_KEY'] = os.urandom(16)
#Code used to initialse the use of emails for the forgot password functionality
# app.config['MAIL_SERVER'] = 'smtp.gmail.com'
# app.config['MAIL_PORT'] = 465
# app.config['MAIL_USE_SSL'] = True
#
# app.config['MAIL_USERNAME'] = 'secure.software.123@gmail.com'
# app.config['MAIL_PASSWORD'] = 'password111.'
#
# mail = Mail(app)

myUsername = "User1"
myToken = "LLL"
allCode = {}
changeEmail = "myEmail@uea.ac.uk"
allCode = {}
currentCookie = "L"

#function which creates randomly generated string (used in CSRF protection)
def randomString(stringLength=25):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

@app.route('/forgotPass')
def newPass():
    return render_template('ForgotPassword.html')


# Used to render the page allowing the user to enter their new password
@app.route('/changePass')
def changePassword():
    return render_template('NewPassword.html')


# A function used to send a recovery email to the user, this will contain a link redirecting the user back to the program's new password page
# @app.route('/sendMail', methods=['GET', 'POST'])
# def sendit():
#     global changeEmail
#     # CHECK IF THE ACCOUNT EXISTS BEFORE SENDING THE EMAIL
#     with sqlite3.connect("initial_test.db") as conn:
#         cursorAccount = conn.cursor()
#
#         # cycles through all the keys, encrypting the entered email address with the xor function, if the encrypted email matches that
#         # which is in the database, we now know that an account with that email address exists, and a recovery email can be sent to this email address
#         allKeys = [myKey[0] for myKey in
#                    cursorAccount.execute("SELECT textkey from users")]  # a list containing all the lists of the users
#         for thisKey in allKeys:  # for loop going through each of the keys in the
#             byted = bytes_xor_int(request.form['email'],
#                                   thisKey)  # pass email and key into bytes_xor function and store output as int 'byted'
#
#             # check database to see if there is an entry where byted exists
#             checkDB = conn.cursor()
#             alreadyExist = checkDB.execute("SELECT * from users where email = ?", (str(byted),)).fetchone()
#             if alreadyExist != None:  # of the account does exist
#                 changeEmail = request.form['email']
#                 myToken = makesalt('salt')  # create a token
#                 theCode = 'Follow the link below to reset your password\nhttp://localhost:5000/reset/' + myToken  # attach the token to the end of the link
#                 msg = Message('Reset password', sender='secure.software.123@gmail.com',
#                               recipients=[changeEmail])  # load the address details for the email
#                 msg.body = theCode
#                 mail.send(msg)  # send the email
#             break
#     message = "If an account with this email exists, a reset link has been set to this email address"
#     return render_template('ForgotPassword.html',
#                            message=message)  # Return the current page to the user with an ambiguous message and an instruction


# Assuming the token matches that which is stored in this session, direct the user to this link by the user clicking the link in the email
@app.route('/reset/<myToken>', methods=['GET', 'POST'])
def resetWithToken(myToken):
    return render_template('NewPassword.html')  # return the page allowing the person to enter the new password


# A function for resetting the paassword of the user, this works by taking in the entered password,
@app.route('/resetMyPass', methods=['GET', 'POST'])
def resetPassword():
    global changeEmail
    # get the information for the form
    password = request.form.get('passwordR')  # get the information for the form
    confirmpassword = request.form.get('confirmpassword')
    if password != confirmpassword:  # checks if the password and the confirm password inputs match, otherwise, the user cannot proceed
        message = "Passwords do not match"
        return render_template('NewPassword.html', message=message)
    else:  # if the user's information matches then proceed with changing of the password
        with sqlite3.connect("initial_test.db") as conn:
            cursorAccount = conn.cursor()
            # cycles through all the keys, encrypting the stored email address (which was entered on the forgotpassword form) with the xor function, if the encrypted email exists then
            # we now know that an account with that email address exists, and the password for that account can be reset
            allKeys = [myKey[0] for myKey in cursorAccount.execute("SELECT textkey from users")]
            for thisKey in allKeys:
                # pass email and key into bytes_xor function and store output as int 'byted'
                byted = bytes_xor_int(changeEmail, thisKey)
                print("The change email: " + changeEmail)
                # check database to see if there is an entry where byted exists
                checkDB = conn.cursor()
                alreadyExist = checkDB.execute("SELECT * from users where email = ?", (
                    str(byted),)).fetchone()  # get the first account where the details match
                if alreadyExist != None:  # if the account does exist
                    # change the password where the email == myEmail
                    conn = sqlite3.connect("initial_test.db")
                    cursor = conn.cursor()
                    # get the salt and salt the entered password
                    mySalt = alreadyExist[7]  # store the salt associated with the user
                    if isinstance(mySalt, str):
                        mySalt = bytes(mySalt, 'utf-8')  # convert it into bytes
                    hashedAndSalt = hashlib.sha512(
                        bytes(password, 'utf-8') + mySalt).hexdigest()  # hash the new password with the salt
                    # store it
                    cursor.execute("UPDATE users SET password = ? WHERE email = ?",
                                   (hashedAndSalt, str(byted)))  # store the new password in the database
                    conn.commit()  # finalise the action above
                    conn.close()  # close the connection
                    return render_template('LoginPage.html')
    message="IDK"
    return render_template('NewPassword.html', message=message)


app.permanent_session_lifetime = timedelta(minutes=5)


# this is opening the homepage
# BLOG HOME PAGE FUNCTION
@app.route('/Home')
def index():
    # global currentCookie
    # print("This Cookie: " + request.cookies.get('session'))
    # print("Current cookie: " + currentCookie)
    # if request.cookies.get('session') == currentCookie:
        if "userID" in allCode:
            conn = sqlite3.connect("initial_test.db")
            cursor = conn.cursor()
            cursor.execute("SELECT * from posts")  # grabs all the posts that have ever been made from the database
            found = cursor.fetchall()  # pulls up them into a list
            if found == None:  # if there were no posts made
                error = 'Invalid Credentials. Please try again.'  #
                print("There are no posts made")
                return render_template('AllPostPage.html', message=error)  # return the page with a message
            else:
                return render_template('AllPostPage.html', data=found)  # return the data to the home page
        else:
            return render_template('LoginPage.html')

@app.route('/')
def startpage():
    if "userID" in allCode:
        return redirect(url_for('home'))  # if the user is logged in, open the home page
    else:
        return render_template('LoginPage.html')


# A function for logging the user out
@app.route('/Logout', methods=['GET', 'POST'])
def logout():
    if "userID" in allCode:
        #pop user
        allCode.pop("userID", None)
    return render_template('LoginPage.html')


# A function for logging the user in
@app.route('/Login', methods=['GET', 'POST'])
def login2():
    error = None
    if request.method == 'POST':  # if the user tries to send something to the server
        global myUsername
        global allCode
        # global currentCookie
        conn = sqlite3.connect("initial_test.db")
        cursor = conn.cursor()
        cursor2 = conn.cursor()
        currentTime = datetime.now()
        now = currentTime.strftime("%d/%m/%Y, %H:%M:%S")  # generate a string of the current date

        cursorAccount = conn.cursor()
        cursorAccount2 = conn.cursor()
        allKeys = [myKey[0] for myKey in
                   cursorAccount.execute("SELECT textkey from users")]  # get all the keys from the database
        for thisKey in allKeys:  # go through all the keys in the table
            allEmails = [theEmail[0] for theEmail in cursorAccount2.execute("SELECT email from users")]
            for anEmail in allEmails:
                # pass email and key into bytes_xor function and store output as int 'byted'
                byted = bytes_xor_int(request.form['email'], thisKey)  # encrypt the entered email with the current key

                if (str(byted) == str(
                        anEmail)):  # if the encrypted entered email matches that of the stored email in the database, an account with this email exists

                    # pull all information from database where this user exists
                    cursor3 = conn.cursor()
                    cursor3.execute(
                        "SELECT forename, surname, email, password, username, salt from users where email = ?",
                        (str(byted),))

                    # get the fetchone on that data, get the salt
                    foundAccount = cursor3.fetchone()  # a command looking for an entry in the database with the same email address
                    sleep(1)

                    if foundAccount == None:
                        error = 'Invalid Credentials. Please try again.'
                        print("There are no results for this query")
                    else:  # if the account does exist, load the information of the user into the session, this can then be accessed at other points on the site
                        enteredPass = bytes(request.form['password'], 'utf-8')
                        storedPass = foundAccount[3]  # get the stored hash representation of the password

                        mySalt = foundAccount[5]
                        allCode["mySalt"] = mySalt
                        if isinstance(mySalt, str):
                            mySalt = bytes(mySalt, 'utf-8')  # if the salt is a string, convert it into a byte so it can be used in the hash function (which only takes in bytes_

                        hashed_entered_password = hashlib.sha512(
                            enteredPass + mySalt).hexdigest()  # hash the entered password text and the stored salt

                        if hashed_entered_password == storedPass:  # if the stored hashed password matches the entered hashed password, then the credentials are correct
                            app.config['SECRET_KEY'] = os.urandom(16)  # generate a new 'secret key' for the application
                            sleep(0.03)

                            # session.permanent = True  # keep the session open

                            # convert all the user information back into plain text and store it in the session, this can be used throughout the rest of the page
                            myUsername = bytes_xor_string(foundAccount[4], thisKey)
                            allCode["userID"] = myUsername
                            allCode["userCode"] = bytes_xor_int(allCode["userID"], thisKey)
                            allCode["forename"] = bytes_xor_string(foundAccount[0], thisKey)
                            allCode["surname"] = bytes_xor_string(foundAccount[1], thisKey)
                            allCode["email"] = bytes_xor_string(foundAccount[2], thisKey)
                            allCode["textDecode"] = thisKey
                            allCode["token"] = randomString()

                            cursor2.execute("UPDATE users SET timestamp = ? WHERE username = ?", (now, allCode["userCode"]))
                            conn.commit()
                            flash('You have successfully logged in!')
                            # resp = make_response(redirect(url_for('home')))
                            # currentCookie = makesalt('salt')
                            # resp.set_cookie('session', currentCookie)
                            # return resp
                            return redirect(url_for('home'))  # send the user to the homepage displaying all the posts
                        conn.close()
                        sleep(0.05)
                        error = 'Invalid Credentials. Please try again.'
                        print("There are no results for this query")
                        return render_template('LoginPage.html', error=error)
    else:
        if "userID" in allCode:
            return redirect(url_for('home'))
    return render_template('LoginPage.html')


# Used to display the registration page
@app.route('/Register', methods=['GET', 'POST'])
def regpage():
    if "userID" in allCode:
        return redirect(url_for('home'))
    else:
        return render_template('RegisterPage.html')

    # Used to display the account dashboard of the user


@app.route('/Account')
def account():
    if "userID" in allCode:
        conn = sqlite3.connect("initial_test.db")
        cursor = conn.cursor()
        cursor2 = conn.cursor()
        cursor.execute(
            "SELECT forename, surname, username, timestamp, textkey from users where username = ?",
            (allCode["userCode"],))  # get the user information associated with the account
        cursor2.execute("SELECT count(*) FROM posts WHERE userID = ?",
                        (allCode["userID"],))  # get the number of posts made

        # As the user already exists and is logged in, there is no need to check where the code below returns 'None'
        found = cursor.fetchone()
        found2 = cursor2.fetchone()

        # Choose the needed information and convert it into a variable to be passed through and returned to the page
        txtForename = bytes_xor_string(found[0], found[4])
        txtSurname = bytes_xor_string(found[1], found[4])
        txtUsername = bytes_xor_string(found[2], found[4])
        print(txtForename)
        print("worked")
        return render_template('AccountPage.html', forename=txtForename, surname=txtSurname, username=txtUsername, timestamp=found[3], postCount=found2[0], token=allCode['token'])
    else:
        return render_template('LoginPage.html')

    # Used to redirect users to the home page


@app.route('/Home')
def home():
    return render_template('AllPostPage.html')


# USERS BLOG LIST FUNCTION
@app.route('/Profile')
def myprofile():
    if "userID" in allCode:  # if the user is logged in
        conn = sqlite3.connect("initial_test.db")
        cursor = conn.cursor()
        cursor.execute("SELECT * from posts where userID = ?",
                       (allCode["userID"],))  # get all the posts made by the logged in user only
        found = cursor.fetchall()
        if found == None:  # if the user has made a post
            message = "There are no posts made"
            return render_template('MyProfilePage.html', message=message)  # return the page with the message
        else:
            return render_template('MyProfilePage.html', data=found)  # return the page with the posts
    else:
        return render_template('LoginPage.html')


# Used to display the page where the user can make a new post
@app.route('/NewPost')
def newpostpage():
    if "userID" in allCode:
        return render_template('NewPostPage.html', token=allCode['token'])
    else:
        return render_template('LoginPage.html')

    # RESET PASSWORD FUNCTION


# @app.route('/ResetPassword', methods=['POST'])
# def resetPass():
#     if request.method == 'POST':
#         error = None
#         # Get the email and check if the email exists
#         email = request.form['email']
#         conn = sqlite3.connect("initial_test.db")
#         cursor = conn.cursor()
#         cursor.execute("SELECT * from users where email = ?", (email,))
#         found = cursor.fetchone()
#         return render_template('LoginPage.html', error=error)


# CREATE NEW USER FUNCTION - REGISTRATION
@app.route('/test2', methods=['POST', 'GET'])
def insertuser2():
    error = None
    if request.method == 'POST':
        error = "All fields must be completed"
        # turning all the entered information into variables
        forename = request.form['forename']
        surname = request.form['surname']
        email = request.form['email']
        username = request.form['username']
        password = request.form['password']
        confirmpassword = request.form['confirmpassword']
        question = request.form.get('securityQ')
        answer = request.form.get('answer')

        if password != confirmpassword:  # check if the passwords are the same, if they are not, do not continue with registration
            error = "Please ensure your passwords match."
            return render_template('RegisterPage.html', error=error)

        theKey = makesalt('key')  # generate a salt - to be used when storing the password
        # Turn all of them into bytes and store them in the db
        forename = str(bytes_xor_int(forename, theKey))
        surname = str(bytes_xor_int(surname, theKey))
        username = str(bytes_xor_int(username, theKey))
        byteQuestion = str(bytes_xor_int(question, theKey))
        byteAnswer = str(bytes_xor_int(answer, theKey))

        # check the database to see if the user already exists
        with sqlite3.connect("initial_test.db") as conn:
            cursorAccount = conn.cursor()
            allKeys = [myKey[0] for myKey in cursorAccount.execute("SELECT textkey from users")]
            for thisKey in allKeys:
                # pass email and key into bytes_xor function and store output as int 'byted'
                byted = bytes_xor_int(email, thisKey)

                # check database to see if there is an entry where byted exists
                checkDB = conn.cursor()
                alreadyExist = checkDB.execute("SELECT * from users where email = ?", (str(byted),)).fetchone()
                if alreadyExist is None:  # if it does then return else
                    print("I AM IN ")
                    with sqlite3.connect("initial_test.db") as conn:
                        c = conn.cursor()
                        # convert the entered information into bytes
                    salt = bytes(makesalt('salt'), 'utf-8')
                    email = str(bytes_xor_int(email, theKey))
                    # hash the password
                    hashedAndSalt = hashlib.sha512(bytes(password, 'utf-8') + salt).hexdigest()
                    # save the information to the database
                    c.execute(
                        "INSERT into users (forename, surname, email, username, password, salt, textkey, securityQuestion, answer) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                        (forename, surname, email, username, hashedAndSalt, salt, theKey, byteQuestion, byteAnswer))
                    conn.commit()
                    conn.close()  # close the database
                    # setting the response
                    return render_template('LoginPage.html')
                else:
                    error = "this user already exists"
                    return render_template('RegisterPage2.html', error=error)
    return render_template('RegisterPage2.html', error=error)

# CREATE NEW POST FUNCTION
@app.route('/MakePost', methods=['POST'])
def newpost():
    if "userID" in allCode:
        testToken = request.form.get('token')
        if allCode['token'] == testToken:
            if request.method == 'POST':
                userID = allCode["userID"] # NEED TO SORT OUT AND SYNCHRONISE WITH LOGIN DETAILS
                title = request.form['title']
                content = request.form['content']

                if (title != '') or (content != ''):
                    with sqlite3.connect("initial_test.db") as conn:
                        c = conn.cursor()
                        c.execute("INSERT into posts (userID, title, content) VALUES (?, ?, ?)",
                                  (userID, title, content))
                    conn.commit()
                    # setting the response
                    conn.close()
                    return index()
                else:
                    error = 'All fields have not be completed. Please try again.'
                return render_template('NewPostPage.html', error=error)
        else:
            attackerror = 'CSRF ATTACK'
            allCode.pop("userID", None)
            return render_template('LoginPage.html', error=attackerror)
    else:
        return render_template('LoginPage.html')


# A function for verifying the user before allowing them to change their password
@app.route('/checkquestion', methods=['GET', 'POST'])
def checkquestion():
    global changeEmail
    # Turns all the entered information into variables
    email = request.form.get('email')
    changeEmail = email
    question = request.form.get('securityQ')
    answer = request.form.get('answer')

    with sqlite3.connect("initial_test.db") as conn:
        cursorAccount = conn.cursor()
        allKeys = [myKey[0] for myKey in cursorAccount.execute("SELECT textkey from users")]
        for thisKey in allKeys:  # go through each of the keys, looking to see if the email exists by encoding the entered info
            # encode all the information
            byteEmail = bytes_xor_int(email, thisKey)
            byteQuestion = bytes_xor_int(question, thisKey)
            byteAnswer = bytes_xor_int(answer, thisKey)

            cursorAccount2 = conn.cursor()
            # execute select statement checking to see if the correct email, question and answer have been entered
            found = cursorAccount2.execute(
                "SELECT * FROM users WHERE email = ? AND securityQuestion = ? AND answer = ?",
                (str(byteEmail), str(byteQuestion), (str(byteAnswer)),)).fetchone()
            if found != None: # if they have then put the user through to the new password page
                return render_template('NewPassword.html')
        # otherwise, send the user a message stating that credentials are incorrect
        message = 'The username or security question is incorrect.'
        return render_template('ForgotPassword.html', message=message)


# CHANGE PASSWORD FUNCTION
@app.route("/changePass/", methods=['POST'])
def updatePass():
    if "userID" in allCode:
        token = request.form.get('token')
        if (allCode['token'] == token):
            try:
                # global myUsername
                conn = sqlite3.connect("initial_test.db")
                cursor = conn.cursor()
                bytePass = bytes(request.form['password'], 'utf-8')
                byteSalt = bytes(allCode["mySalt"], 'utf-8')
                convertedPass = hashlib.sha512(bytePass + byteSalt).hexdigest()
                cursor.execute("UPDATE users set password  = ? where username = ?",
                               (convertedPass, allCode["userCode"]))
                conn.commit()
                print("It worked: password")
                return index()
            except sqlite3.Error as error:
                print("Failed to update password sqlite table", error)
                error = "Failed to update password in sqlite table"
                return render_template('AccountPage.html', error=error)
            finally:
                if(conn):
                    conn.close()
                    print("The connection has been closed.")
            return render_template('AccountPage.html')
        else:
            error = 'CSRF ATTACK'
            allCode.pop("userID", None)
            return render_template('LoginPage.html', error=error)
    else:
        return render_template('LoginPage.html')


# CHANGE USERNAME FUNCTION
@app.route("/changeUser/", methods=['POST'])
def updateUser():
    if "userID" in allCode:
        token = request.form.get('token')
        if (allCode['token'] == token):
            try:
                # global myUsername
                conn = sqlite3.connect("initial_test.db")
                cursor = conn.cursor()
                convertedUser = bytes_xor_int(request.form['username'], allCode["textDecode"])
                cursor.execute("UPDATE users set username  = ? where username = ?",
                               (convertedUser, allCode["userCode"]))
                #update all users that have the same username
                cursor2 = conn.cursor()
                cursor2.execute("UPDATE posts set userID  = ? where userID = ?",
                                (request.form['username'], allCode["userID"]))
                allCode["userID"] = request.form['username']
                allCode["userCode"] = convertedUser
                conn.commit()
                print("It worked: username")

            except sqlite3.Error as error:
                print("Failed to update username sqlite table", error)
                error = "Failed to update username in sqlite table"
                return render_template('AccountPage.html', error=error)
            finally:
                if(conn):
                    conn.close()
                    print("The connection has been closed.")
            return index()
        else:
            error = 'CSRF ATTACK'
            allCode.pop("userID", None)
            return render_template('LoginPage.html', error=error)
    else:
        return render_template('LoginPage.html')


# Function of getting the number of posts made by the user
# @app.route("/postCount/", methods=['GET'])
# def postCounter():
#     global myUsername
#     conn = sqlite3.connect("initial_test.db")
#     cursor = conn.cursor()
#     cursor.execute("SELECT count(*) FROM posts WHERE userID = ?", (allCode["userID"],))
#     found = cursor.fetchone()
#     if found == None:
#         return render_template('AccountPage.html', msg1='No posts found')
#     else:
#         return render_template('AccountPage.html', data2=found)


# A function for converting a string into a integer (converted from a byte sequence)
def bytes_xor_int(words, key):
    # convert email into byte format
    byteWord = bytes(words, 'utf-8')  # converts the 'word' string into a byte
    # convert key into byte format
    byteKey = bytes(key, 'utf-8')  # converts the 'key' string into a byte
    # return byte XOR of entered info
    byted = bytes(x ^ y for x, y in zip(byteWord, byteKey))  # converts the 'key' string into a byte
    intEncoded = int.from_bytes(byted, byteorder='big')  # convert byte object into integer equivalent
    return intEncoded  # return integer


# A function for converting a integer (converted from a byte sequence) into a string
def bytes_xor_string(intEncoded, key):
    intEncoded = int(intEncoded)  # converts the entered value into an integer
    byteEncoded = intEncoded.to_bytes((intEncoded.bit_length() // 8) + 1,
                                      byteorder='big')  # converts the integer value into byte with a big endian byteorder
    byteKey = bytes(key, 'utf-8')  # converts the 'key' string into a byte
    convertToString = bytes(
        x ^ y for x, y in zip(byteEncoded, byteKey))  # execute the xor operation on the integer and the key vlaues
    converted = "".join(chr(x) for x in convertToString)  # convert each of the bytes into strings
    return converted  # return the string


# A function for generating a set of random character
def makesalt(dataTyped):
    saltALPHABET = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    keyALPHABET = "abcdefghijklmnopqrstuvwxyz"
    chars = []
    if dataTyped == 'salt':
        return ''.join(random.choice(saltALPHABET) for i in range(32))
    elif dataTyped == 'key':
        return ''.join(random.choice(keyALPHABET) for i in range(32))


if __name__ == "__main__":
    app.run()
