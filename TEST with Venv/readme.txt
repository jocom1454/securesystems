Secure Group Application v.1.0

Contents
========
- General description
- Instructions
- Contributors and contact details


General description
===================

This is a microblogging site in which the user can make posts, 
see their posts, manage their account details and view the posts of other users on the site.
Measures have been taken to secure the site against SQL Injection, 
Cross-Site Scripting, Cross-Site Request Forgery, Session hi-jacking and 
Account Enumeration among other vulnerabilities.


Instructions
============

- To start the program, launch the command line and locate the folder.
- Launch the website by first starting the server, do this by typing 'python flaskManager.py'.
- Go to your browser and type the address: 'http://127.0.0.1:5000/'. Now you can start using the site.

- In order to use the site, you must have an account and will therefore need to register.
- After registration, the user can log in and use the microblogging site.


Contributors and contact details
============

- Michael Oterunbi - jkq17fvu@uea.ac.uk
- Joseph Agagwuncha - ukk17cbu@uea.ac.uk
- Michael Duke - hmy18awu@uea.ac.uk